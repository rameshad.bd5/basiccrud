
<?php include('partials/header.php'); ?>

<!-- Start Nabtop section -->
<?php include('partials/topnav.php'); ?>
<!-- End Nabtop section -->

<!-- Start navber section -->
<?php include('partials/navber.php'); ?>
<!-- End navber section -->

<!-- Start New Massege -->
<section>
	<div class="container">
		<?php 
			session_start();
			if(isset($_SESSION["mas"])){
				echo $_SESSION["mas"];
			}
			session_destroy();
		?>
	</div>
</section>
<!-- Start New Massege -->

<!-- Start Registration Form -->
<section>
	<div class="reg-form">
		<div class="container">
			<h2>Registration Form </h2>
			<div class="row">
				<div class="col-md-6">
					<form action="myphp/save.php" method="POST">
					  <div class="form-group">
					    <label for="fname">First Name:</label>
					    <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname">
					  </div>
					  <div class="form-group">
					    <label for="lname">Last Name:</label>
					    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname">
					  </div>
					  <div class="form-group">
					    <label for="email">Email Address:</label>
					    <input type="email" class="form-control" id="email" placeholder="Email Address" name="email">
					  </div>

					  <button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
				<div class="col-md-6">
					
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Registration Form -->

<?php include('partials/footer.php'); ?>